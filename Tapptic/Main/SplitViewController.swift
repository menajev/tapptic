//
//  SplitViewController.swift
//  Tapptic
//
//  Created by PMencfeld on 01/09/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.preferredDisplayMode = UIDevice.current.userInterfaceIdiom == .pad ? .automatic : .allVisible
    }
    
    func splitViewController(
        _ splitViewController: UISplitViewController,
        collapseSecondary secondaryViewController: UIViewController,
        onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}

