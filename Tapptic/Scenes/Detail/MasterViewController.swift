//
//  MasterViewController.swift
//  Tapptic
//
//  Created by PMencfeld on 31/08/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import UIKit
import SDWebImage

class MasterViewController: UITableViewController {
    var detailViewController: DetailViewController? = nil
    lazy var interactor = MasterInteractor(viewController: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count - 1] as? UINavigationController)?.topViewController as? DetailViewController
        }
        
        interactor.loadList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func reloadData() {
        tableView.reloadData()
        
        if(interactor.list.count > 1) {
            DispatchQueue.main.async {
                self.detailViewController?.model = self.interactor.list[0]
                self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
            }
        }
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as? DetailViewController
                controller?.model = interactor.list[indexPath.row]
                detailViewController = controller
            }
        }
    }
    
    // MARK: - Table View
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.list.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = interactor.list[indexPath.row].name
        cell.imageView?.sd_setImage(with: interactor.list[indexPath.row].imageUrl)
        
        let bgView = UIView()
        bgView.backgroundColor = .red
        cell.selectedBackgroundView = bgView
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        setCell(at: indexPath, backgroundColor: .blue, textColor: .white)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setCell(at: indexPath, backgroundColor: .red, textColor: .white)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        setCell(at: indexPath, backgroundColor: .clear, textColor: .black)
    }
    
    override func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        setCell(at: indexPath, backgroundColor: .clear, textColor: .black)
    }
    
    private func setCell(at indexPath: IndexPath, backgroundColor: UIColor, textColor: UIColor) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.selectedBackgroundView?.backgroundColor = backgroundColor
        cell?.textLabel?.textColor = textColor
    }
    
}
