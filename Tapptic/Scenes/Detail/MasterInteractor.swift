//
//  MasterInteractor.swift
//  Tapptic
//
//  Created by PMencfeld on 01/09/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import Foundation

class MasterInteractor {
    let apiClient = ApiClient()
    let viewController: MasterViewController
    var list = [ItemModel]()
    
    init(viewController: MasterViewController) {
        self.viewController = viewController
    }
    
    func loadList() {
        apiClient.getList(completion: { [weak self] list, error in
            self?.list = list ?? []
            DispatchQueue.main.async {
                self?.viewController.reloadData()
            }
        })
    }
}
