//
//  DetailInteractor.swift
//  Tapptic
//
//  Created by PMencfeld on 01/09/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import Foundation

class DetailInteractor {
    let viewController: DetailViewController
    var item: ItemModel?
    var itemDetail: ItemDetailModel?
    
    init(viewController: DetailViewController) {
        self.viewController = viewController
    }
    
    func loadDetail(item: ItemModel?) {
        guard let item = item else { return }
        
        viewController.showLoading()
        let apiClient = ApiClient()
        
        apiClient.getItemDetails(item: item, completion: { [weak self] itemDetail, error in
            self?.itemDetail = itemDetail
            DispatchQueue.main.async {
                if let itemDetail = itemDetail {
                self?.viewController.setup(detail: itemDetail)
                } else {
                    self?.viewController.showError(error: error?.localizedDescription ?? "Unkown error")
                }
            }
        })
    }
}
