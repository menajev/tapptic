//
//  DetailViewController.swift
//  Tapptic
//
//  Created by PMencfeld on 31/08/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var iconImageView: UIImageView?
    lazy var interactor = DetailInteractor(viewController: self)

    var model: ItemModel? {
        didSet {
            interactor.loadDetail(item: model)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setup(detail: ItemDetailModel) {
        nameLabel?.text = detail.text
        iconImageView?.sd_setImage(with: detail.imageUrl)
    }
    
    func showLoading() {
        nameLabel?.text = "Loading..."
        iconImageView?.image = nil
    }

    func showError(error: String) {
        nameLabel?.text = error
        iconImageView?.image = nil
    }

}

