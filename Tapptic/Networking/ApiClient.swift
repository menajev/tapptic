//
//  ApiClient.swift
//  Tapptic
//
//  Created by PMencfeld on 31/08/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import Foundation

enum ApiError: Error {
    case parseError
}

class ApiClient {
    
    private let basePath = "http://dev.tapptic.com/test/"
    
    private func request(path: String, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        guard let url = URL(string: basePath + path) else { return }
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            completion(data, error)
        })
        task.resume()
    }
    
    func getList(completion: @escaping (_ list: [ItemModel]?, _ error: Error?) -> Void) {
        request(path: "json.php", completion: { data, error in
            if let data = data {
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode([ItemModel].self, from: data)
                    completion(response, nil)
                } catch {
                    completion(nil, ApiError.parseError)
                }
            } else {
                completion(nil, error)
            }
        })
    }
    
    func getItemDetails(item: ItemModel, completion: @escaping (_ detail: ItemDetailModel?, _ error: Error?) -> Void) {
        request(path: String(format: "json.php?name=%@", item.name), completion: { data, error in
            if let data = data {
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode(ItemDetailModel.self, from: data)
                    completion(response, nil)
                } catch {
                    completion(nil, ApiError.parseError)
                }
            } else {
                completion(nil, error)
            }
        })
    }
    
}


