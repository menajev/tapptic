//
//  ItemModel.swift
//  Tapptic
//
//  Created by PMencfeld on 31/08/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import Foundation

class ItemModel: Codable {
    let name: String
    let image: String
    
    var imageUrl: URL? {
        URL(string: image)
    }
}
