//
//  ItemDetailModel.swift
//  Tapptic
//
//  Created by PMencfeld on 01/09/2020.
//  Copyright © 2020 PMencfeld. All rights reserved.
//

import Foundation

class ItemDetailModel: Codable {
    let name: String
    let text: String
    let image: String
    
    var imageUrl: URL? {
        URL(string: image)
    }
}

